#!/bin/bash


date=$(date)
result="/home/ubuntu/result.txt"
rm -rf $result #очистим файл для нового сканирования


echo "Привет. Данный скрипт позволяет получить отчет об открытых портах на серверах из списка."
echo -n "Введите путь к списку, например, /home/ubuntu/list.txt "
read file
echo "Вы указали путь $file. Принято!"


echo ""
echo -n "Введите ваш email для получения отчета сканирования: "
read email
echo "Ваш ящик $email. Принято!"

################################################################### проверить, есть ли в системе nmap

nmap="$(dpkg -s nmap | grep "ok installed")"
if  [[ "$nmap" == *"ok installed"* ]];
then echo "NMAP check - ok!"
else

echo ""
echo "Для работы сканера нужна утилита NMAP для сканирования."
echo -n "Установить её автоматически? (y/n) "
read check
case "$check" in
y|Y) echo "NMAP installing..."
sudo apt-get install -y nmap
;;
n|N) echo "Нет так нет. Если передумаете, запустите скрипт еще раз. До встречи!"
exit 0
;;
*) echo "Вы вводите что-то не то. Пожалуйста, запустите скрипт заново."
exit 0
;;
esac

fi

################################################################### проверить, есть ли в системе postfix

mailutils="$(dpkg -s mailutils | grep "ok installed")"
if  [[ "$mailutils" == *"ok installed"* ]];
then echo "POSTFIX mailutils check - ok!"

else
echo ""
echo "Для отправки отчета сканирования нужна утилита POSTFIX."
echo -n "Установить её автоматически? (y/n) "
read check
case "$check" in
y|Y) echo "POSTFIX installing..."
sudo apt-get install -y mailutils
sudo apt-get install -y nmap
;;
n|N) echo "Нет так нет. Если передумаете, запустите скрипт еще раз. До встречи!"
exit 0
;;
*) echo "Вы вводите что-то не то. Пожалуйста, запустите скрипт заново."
exit 0
;;
esac
fi

###################################################################
echo "Всё готово. Сканируем порты..."

for var in $(cat $file) #делаем цикл, так как применяем ту же команду для списка сканируемых хостов.
#Nmap умеет сканить из файла командой #nmap -iL list.txt, но в текущей реализации скрипт более гибкий, если мы захотим использовать другие инструменты сканирования.
#В данной реализации используем TCP сканирование.
do
echo "✓ $var">> $result
nmap -sT $var | grep open | tee -a $result
echo "Scan date: $date">> $result
echo -en "\n" >> $result

done

cat $result | mail -s "Servers portscan results" -r "admin@portscan.results" $email #отправляем результат на указанную почту
echo ""
echo "Всё готово. Отчет по сканированию уже на вышем почтовом ящике."
echo "Обратите внимание, что письмо могло попасть в СПАМ."
echo "Копия отчета лежит по адресу /home/ubuntu/result.txt. Вдруг пригодится)"
